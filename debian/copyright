Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xdvik
Source: http://xdvi.sourceforge.net/
Files-Excluded: BUGS
 Build
 CHANGES
 ChangeLog
 INSTALL
 Makefile.am
 Makefile.in
 README
 README.0overview
 README.1prerequisites
 README.2building
 README.3installing
 README.4layout
 README.5configure
 README.6cross
 README.7coding
 README.CVS
 README.xdvik
 README_maintainer
 TODO
 aclocal.m4
 am
 auxdir
 build-aux
 configure
 configure.ac
 libs
 reautoconf
 tardate.ac
 texk/ChangeLog
 texk/Makefile.am
 texk/Makefile.in
 texk/README
 texk/aclocal.m4
 texk/configure
 texk/configure.ac
 texk/kpathsea
 utils
 version.ac
Comment: Stirp TeXLive related files from original sources for duplication

Files: *
Copyright: 1999-2014 Paul Vojta and the xdvik development team
License: Expat

Files: m4/*
Copyright: 2009-2012 Peter Breitenlohner <tex-live@tug.org>
License: permssive
 You may freely use, modify and/or distribute this file.

Files: texk/xdvik/encodings.c
Copyright: 1999 Antti-Juhani Kaijanaho <gaia@iki.fi>
           2001 Bjoern Brill <brill@fs.math.uni-frankfurt.de>
           1999-2001 Free Software Foundation, Inc.
           2003-2013 the xdvik development team
License: GPL-2.0+

Files: debian/*
Copyright: 1999 Hayao Nakahara <nakahara AT debian.org>
           2002 Masayuki Hatta <mhatta AT debian.org>
           2010 TSUCHIYA Masatoshi <tsuchiya AT namazu.org>
           2010 Atsuhito Kohda <kohda AT debian.org>
           2010-2020 Youhei SASAKI <uwabami@gfd-dennou.org>
License: GPL-2.0+

Files: debian/patches/0001-Update-pTeX-patch-AJ16.c.patch
Copyright: 2011-2013 Hironori KITAGAWA
           2013 the Xdvik-jp project
License: Expat

Files: debian/patches/0002-Update-pTeX-patch-ft2vert.patch
Copyright: 2005 Nobuyuki Tsuchimura
           2013 the Xdvik-jp project
License: LGPL-2.0+

Files: debian/patches/0003-Update-pTeX-patch-ptexvf.patch
Copyright: 1993 Yasuhisa Hayashi
           2005 Nobuyuki Tsuchimura
           2013 the Xdvik-jp project
License: Expat

Files: debian/patches/0004-Update-pTeX-patch-jfm.patch
Copyright: 1987 Atsuo Kawaguchi
           1987 Yasuhisa Hayashi
           2013 the Xdvik-jp project
License: Expat

Files: debian/patches/0005-Update-pTeX-patch-ptexmap.patch
Copyright: 1996 Masahito Yamaga
           2008 Nobuyuki Tsuchimura
           2013 the Xdvik-jp project
License: Expat

Files: debian/patches/0006-Update-pTeX-patch-fontconfig.patch
Copyright: 2009 Nobuyuki Tsuchimura
           2013 the Xdvik-jp project
License: Expat

Files: debian/patches/0007-Update-pTeX-patch-ft2.patch
Copyright: 2002 Otofuji
           2013 the Xdvik-jp project
License: Expat

Files: debian/patches/0008-Update-pTeX-patch-xdvik-jp.patch
Copyright: 2013 the Xdvik-jp project
License: Expat

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.0+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
